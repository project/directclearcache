CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers


INTRODUCTION
------------

This module provides you with a button on Admin Menu to clear cache.
To clear cache in Drupal you have to navigate Configuration->Performance->Clear All Caches.
But with this module, you can get a button on Admin Menu to clear cache and Save time.

REQUIREMENTS
------------

No special requirements.



INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * Mohammad Waseem - https://www.drupal.org/u/response2waseem
